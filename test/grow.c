#include <clint/clint.h>
#include "test.h"

int main()
{
    clint_t* a = clint_alloc();
    test(a != NULL, 1, "alloc failed");
    clint_grow(a, clint_min_buffer);
    test(a->max == clint_min_buffer, 2, "grew when it shouldn't have");
    clint_grow(a, clint_min_buffer+1);
    test(a->max == 2*clint_min_buffer, 3, "didn't grow");
    clint_grow(a, clint_min_buffer);
    test(a->max == 2*clint_min_buffer, 4, "grew when it shouldn't have");
    clint_sfree(&a);
}

