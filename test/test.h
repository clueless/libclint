#include <stdio.h>
#include <stdlib.h>

void test(int expr, int ret, const char* msg)
{
    if (!expr)
    {
        puts(msg);
        printf("exit code: %d\n", ret);
        exit(ret);
    }
}

