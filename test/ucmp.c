#include <clint/clint.h>
#include "test.h"

int main()
{
    int ret = 0;
    clint_t* a = clint_alloc();
    clint_t* b = clint_alloc();
    test(a != NULL && b != NULL, ++ret, "alloc failed");
    
    // opposite signs, same value
    test(clint_set_n(a, 0xFF0000000000LL) == clint_ok, ++ret, "set_n failed");
    test(clint_set_n(b, -0xFF0000000000LL) == clint_ok, ++ret, "set_n failed");
    test(clint_ucmp(a, b) == clint_equal, ++ret, "ucmp failed");
    
    // a smaller magnitude than b
    test(clint_set_n(a, 0xFFLL) == clint_ok, ++ret, "set_n failed");
    test(clint_set_n(b, 0xFF0000000000LL) == clint_ok, ++ret, "set_n failed");
    test(clint_ucmp(a, b) == clint_less, ++ret, "ucmp failed");
    
    // b smaller magnitude than a
    test(clint_set_n(a, 0xFF0000000000LL) == clint_ok, ++ret, "set_n failed");
    test(clint_set_n(b, 0xFFLL) == clint_ok, ++ret, "set_n failed");
    test(clint_ucmp(a, b) == clint_greater, ++ret, "ucmp failed");
    
    // Same magnitude, a smaller
    test(clint_set_n(a, 0xFF0000000000LL) == clint_ok, ++ret, "set_n failed");
    test(clint_set_n(b, 0xFF0000000001LL) == clint_ok, ++ret, "set_n failed");
    test(clint_ucmp(a, b) == clint_less, ++ret, "ucmp failed");
    
    // Same magnitude, b smaller
    test(clint_set_n(a, 0xFF0000000001LL) == clint_ok, ++ret, "set_n failed");
    test(clint_set_n(b, 0xFF0000000000LL) == clint_ok, ++ret, "set_n failed");
    test(clint_ucmp(a, b) == clint_greater, ++ret, "ucmp failed");
}
