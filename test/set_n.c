#include <clint/clint.h>
#include "test.h"

int main()
{
    clint_t* a = clint_alloc();
    test(a != NULL, 1, "alloc failed");


    clint_set_n(a, 1 << (clint_single_radix - 1));
    int expr = 
        a->size == 1 &&
        a->sign == clint_positive &&
        a->buffer[0] == 1llu << (clint_single_radix - 1);
    test(expr, 2, "setn incorrect");


    clint_set_n(a, 1llu << clint_single_radix);
    expr = 
        a->size == 2 &&
        a->sign == clint_positive &&
        a->buffer[0] == 0 &&
        a->buffer[1] == 1;
    test(expr, 3, "setn incorrect");

    clint_set_n(a, -1ll*(1llu << clint_single_radix));
    expr = 
        a->size == 2 &&
        a->sign == clint_negative &&
        a->buffer[0] == 0 &&
        a->buffer[1] == 1;
    test(expr, 4, "setn incorrect");
    
    clint_set_n(a, 0);
    expr = 
        a->size == 0 &&
        a->sign == clint_positive &&
        a->buffer[0] == 0 &&
        a->buffer[1] == 0;
    test(expr, 5, "setn incorrect");
    clint_sfree(&a);
}

