#include <clint/clint.h>
#include "test.h"

int main()
{
    clint_t* i = clint_alloc();
    test(i != NULL, 1, "alloc failed");

    // Zero
    clint_trim(i);
    test(i->size == 0, 2, "trim failed");
   
    
    // Zero with leading zeroes
    i->size = clint_min_buffer;
    clint_trim(i);
    test(i->size == 0, 3, "trim failed");
   

    // digits with leading zeroes
    i->buffer[0] = 1;
    i->buffer[1] = 1;
    i->size = clint_min_buffer;
    clint_trim(i);
    test(i->size == 2, 4, "trim failed");
    
    // Digits without leading zeroes
    clint_trim(i);
    test(i->size == 2, 5, "trim failed");

    clint_sfree(&i);
}
