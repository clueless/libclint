#include <clint/clint.h>
#include "test.h"

int main()
{
    clint_t* a = clint_alloc();
    test(a != NULL, 1, "alloc failed");
    clint_sfree(&a);
    test(a == NULL, 2, "sfree failed");
}
