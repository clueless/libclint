#ifndef CLINT_CLINT_H
#define CLINT_CLINT_H

#include <clint/config.h>

typedef struct clint_ {
    int size;
    int max;
    int sign;
    clint_single_t* buffer; 
} clint_t;

clint_t* clint_alloc();
clint_t* clint_alloc_size(int);
clint_t* clint_alloc_copy(clint_t*);
void clint_free(clint_t*);
void clint_sfree(clint_t**);

int clint_grow(clint_t*, int);
void clint_trim(clint_t*);

int clint_ucmp(clint_t*, clint_t*);
int clint_cmp(clint_t*, clint_t*);
void clint_zero(clint_t*);
int clint_abs(clint_t*, clint_t*);
int clint_neg(clint_t*, clint_t*);
int clint_set(clint_t*, clint_t*);
int clint_set_n(clint_t*, int64_t);
int clint_uadd(clint_t*, clint_t*, clint_t*);
#endif
