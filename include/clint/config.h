#ifndef CLINT_CONFIG_H
#define CLINT_CONFIG_H

#include <stdint.h>

typedef uint32_t clint_single_t;
typedef uint64_t clint_double_t;

static const int clint_single_radix = 31;
static const int clint_double_radix = 32;

static const clint_single_t clint_single_mask = 0x7FFFFFFF;

static const int clint_min_buffer = 16;
static const int clint_negative = -1;
static const int clint_positive = 1;

static const int clint_ok = 0;
static const int clint_error_memory = 1;

static const int clint_less = -1;
static const int clint_equal = 0;
static const int clint_greater = 1;
#endif
