#include <clint/clint.h>

int clint_abs(clint_t* dst, clint_t* src)
{
    int ret = clint_set(dst, src);
    if (ret != clint_ok)
        return ret;

    dst->sign = clint_positive;
    return clint_ok;
}
