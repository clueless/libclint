#include <string.h>
#include <stdlib.h>
#include <clint/clint.h>

#include "fixlen.h"

int clint_grow(clint_t* i, int len)
{
    if (i->max >= len)
        return clint_ok;
    
    len = fixlen(i->max, len);
    void* new = calloc(sizeof(clint_single_t), len);
    if (new == NULL)
        return clint_error_memory;
    
    memcpy(new, i->buffer, i->size);
    free(i->buffer);
    i->buffer = new;
    i->max = len;
    return clint_ok;
}
