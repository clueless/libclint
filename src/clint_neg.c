#include <clint/clint.h>

int clint_neg(clint_t* dst, clint_t* src)
{
    int ret = clint_set(dst, src);
    if (ret != clint_ok)
        return ret;
    
    if (dst->size > 0)
        dst->sign *= clint_negative;
    return clint_ok;
}
