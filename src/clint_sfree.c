#include <stddef.h>
#include <clint/clint.h>

void clint_sfree(clint_t** i)
{
    clint_free(*i);
    *i = NULL;
}
