#include <clint/clint.h>

int clint_cmp(clint_t* a, clint_t* b)
{
    if (a->sign != b->sign)
        return a->sign == clint_positive ? clint_greater : clint_less;
    
    
    if (a->sign == clint_positive)
        return clint_ucmp(a, b);
    else
        return clint_ucmp(b, a);
}
