#include <string.h>
#include <stdlib.h>
#include <clint/clint.h>

#include "fixlen.h"

clint_t* clint_alloc_size(int len)
{
    clint_t* out = malloc(sizeof(clint_t));
    if (out == NULL)
        return NULL;
    
    len = fixlen(clint_min_buffer, len);
    out->buffer = calloc(sizeof(clint_single_t), len);
    if (out->buffer == NULL)
        goto cleanup;
    
    out->size = 0;
    out->max = len;
    out->sign = clint_positive;

    return out;

cleanup:
    free(out);
    return NULL;
}

