#include <string.h>
#include <stdlib.h>
#include <clint/clint.h>

clint_t* clint_alloc_copy(clint_t* in)
{
    clint_t* out = malloc(sizeof(clint_t));
    if (out == NULL)
        return NULL;
    
    out->buffer = malloc(sizeof(clint_single_t) * in->max);
    if (out->buffer == NULL)
        goto cleanup;
    
    out->size = in->size;
    out->max = in->max;
    out->sign = in->sign;
    memcpy(out->buffer, in->buffer, sizeof(clint_single_t) * in->max);

    return out;

cleanup:
    free(out);
    return NULL;
}
