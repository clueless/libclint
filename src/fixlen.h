#ifndef SRC_CLINT_FIXLEN_H
#define SRC_CLINT_FIXLEN_H

static inline int fixlen(int current, int desired)
{
    // TODO: Overflow protection, less naive growth
    while(current < desired)
        current *= 2;
    return current;
}


#endif
