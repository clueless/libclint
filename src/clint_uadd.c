#include <clint/clint.h>

int clint_uadd(clint_t* dst, clint_t* a, clint_t* b)
{
    clint_t *max;
    clint_t *min;
    if (a->size > b->size)
    {
        max = a;
        min = b;
    }
    else
    {
        max = b;
        min = a;
    }
    
    if (dst->max < (max->size + 1))
    {
        int ret = clint_grow(dst, max->size + 1);
        if (ret != clint_ok)
            return ret;
    }

    int carry = 0;
    for (int i = 0; i < min->size; i++)
    {
        clint_single_t tmp;
        tmp = min->buffer[i] + max->buffer[i] + carry;
        carry = tmp >> clint_single_radix;
        dst->buffer[i] = tmp & clint_single_mask;
    }

    for (int i = min->size; i < max->size; i++)
    {
        clint_single_t tmp;
        tmp = max->buffer[i] + carry;
        carry = tmp >> clint_single_radix;
        dst->buffer[i] = tmp & clint_single_mask;
    }

    if (carry)
    {
        dst->buffer[max->size] = carry;
        dst->size = max->size + 1;
    }
    else
    {
        dst->size = max->size;
    }

    return clint_ok;
}
