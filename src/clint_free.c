#include <string.h>
#include <stdlib.h>
#include <clint/clint.h>

void clint_free(clint_t* i)
{
    if (i == NULL)
        return;

    free(i->buffer);
    memset(i, 0, sizeof(clint_t));
    free(i);
}
