#include <string.h>
#include <clint/clint.h>

void clint_zero(clint_t* i)
{
    memset(i->buffer, 0, i->size * sizeof(clint_single_t));
    i->size = 0;
    i->sign = clint_positive;
}
