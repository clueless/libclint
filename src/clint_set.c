#include <clint/clint.h>

static inline int64_t iabs(int64_t n)
{
    if (n < 0)
        return n * -1;
    else
        return n;
}

static inline int sign(int64_t n)
{
    if (n < 0)
        return clint_negative;
    else
        return clint_positive;
}


int clint_set_n(clint_t* i, int64_t n)
{
    clint_zero(i);
    int ret = clint_grow(i, 63/clint_single_radix + 1);
    if (ret != clint_ok)
        return ret;

    i->sign = sign(n);
    n = iabs(n);

    while (n)
    {
        clint_single_t digit = n & clint_single_mask;
        n >>= clint_single_radix;
        i->buffer[i->size++] = digit;
    }
    return clint_ok;
}
