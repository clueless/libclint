#include <clint/clint.h>

void clint_trim(clint_t* i)
{
    while (i->size && i->buffer[i->size - 1] == 0)
        i->size--;
}
