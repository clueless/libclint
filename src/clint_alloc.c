#include <string.h>
#include <stdlib.h>
#include <clint/clint.h>

clint_t* clint_alloc()
{
    clint_t* i = malloc(sizeof(clint_t));
    if (i == NULL)
        return NULL;
    
    i->buffer = calloc(sizeof(clint_single_t), clint_min_buffer);
    if (i->buffer == NULL)
        goto cleanup;
    
    i->size = 0;
    i->max = clint_min_buffer;
    i->sign = clint_positive;

    return i;

cleanup:
    free(i);
    return NULL;
}
