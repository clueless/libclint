#include <clint/clint.h>

int clint_ucmp(clint_t* a, clint_t* b)
{
    if (a->size > b->size)
        return clint_greater;
    else if (b->size > a->size)
        return clint_less;
    
    if (a->size == 0)
        return clint_equal;

    int i = a->size;
    while (--i >= 0 && a->buffer[i] == b->buffer[i]);

    if (i < 0)
        return clint_equal;
    
    if (a->buffer[i] > b->buffer[i])
        return clint_greater;
    else
        return clint_less;
}
