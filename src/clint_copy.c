#include <string.h>
#include <clint/clint.h>

int clint_set(clint_t* dst, clint_t* src)
{
    if (dst == src)
        return clint_ok;
    
    int ret = clint_grow(dst, src->size);
    if (ret != clint_ok)
        return ret;

    dst->size = src->size;
    dst->sign = src->sign;
    memcpy(dst->buffer, src->buffer, dst->size * sizeof(clint_single_t));
    return clint_ok;
}
