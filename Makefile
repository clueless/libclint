.PHONY: clean all tests

AR=ar
CC=gcc
LD=gcc

ARFLAGS=rcu
CCFLAGS=-std=c11 -Wall -Wextra -Iinclude -fPIC
LDFLAGS=-shared -fPIC
LDLIBS=

SOURCES=$(shell find src -type f -name \*.c)
OBJS=$(patsubst src/%, obj/%.o, $(SOURCES))

all: lib/libclint.so lib/libclint.a

lib/libclint.so: obj $(OBJS) lib
	$(LD) $(LDFLAGS) -o $@ $(OBJS) $(LDLIBS)

lib/libclint.a: obj $(OBJS) lib
	$(AR) $(ARFLAGS) $@ $(OBJS)

obj/%.c.o: src/%.c
	$(CC) -c $^ $(CCFLAGS) -o $@

obj:
	mkdir obj $(OBJ_TREE)

lib:
	mkdir lib

clean:
	rm -rf obj
	rm -rf lib
	cd test && make clean

tests:
	cd test && make clean all
